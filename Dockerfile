FROM ubuntu:18.10

# setup ubuntu dependencies to build the commandline tools
RUN export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && \
    echo "tzdata tzdata/Areas select Europe" >> preseed.txt && echo "tzdata tzdata/Zones/Europe select Berlin" >> preseed.txt && \
    debconf-set-selections preseed.txt && \
    apt-get update && apt-get install -y --no-install-recommends qt5-default libopencv-dev git &&\
    apt-get clean

RUN apt-get install -y imagemagick
RUN apt-get install -y python3-pip
RUN pip3 install wheel setuptools && pip3 install wand pyaml qrcode pillow

COPY policy.xml /etc/ImageMagick-6/policy.xml

WORKDIR /WORKDIR

